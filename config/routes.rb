Mysimpleplants::Application.routes.draw do

  devise_for :growers
  get 'registrations/thanks', controller: 'registrations', action: 'thanks'
  resources :registrations

  resources :orders

  resources :option_types

  resources :steps

  resources :line_item_options

  resources :line_items

  get '/carts/update_totals', to: 'carts#update_totals'
  get '/carts/update_quantity', to: 'carts#update_quantity'
  get '/carts/update_hated_plant', to: 'carts#update_hated_plant'
  get '/mycart' => 'carts#show'
  get '/growers/home', to: 'growers#show'
  get '/growers/edit_info', to: 'growers#edit_info'
  get '/growers/orders/:order_id', to: 'growers#show_order', as: 'growers_show_order'
  post '/growers/order_status_picked', to: 'growers#order_status_picked' 
  post '/growers/order_status_complete', to: 'growers#order_status_complete'
  post '/growers/master_picking_sheet', to: 'growers#master_picking_sheet'
  post '/growers/picking_sheets', to: 'growers#picking_sheets'
  get '/growers/shipping_label/:order_id', to: 'growers#shipping_label', as: 'growers_shipping_label'
  post '/growers/table', to: 'growers#table'
  post '/growers/totals', to: 'growers#totals'
  resources :growers
  resources :carts
  resources :pages
  resources :options

  resources :option_sets

  resources :grower_zip_codes

  get '/zip_codes/check_availability', to: 'zip_codes#check_availability'
  resources :zip_codes

  resources :zones

  resources :products
  get '/buy-plants', to: 'products#show', as: 'buy_plants', defaults: { id: 1 }

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :contacts

  #resources :growers do
  #  resources :orders do
  #    get :shipping_label
  #  end
  #end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pages#show', defaults: { id: 8 }

  get 'register', to: 'registrations#new', as: 'register'
  get 'about-us', to: 'pages#show', as: 'about_us', defaults: { id: 2 }
  get 'how-it-works', to: 'pages#show', as: 'hiw', defaults: { id: 1 }
  get 'our-growers', to: 'pages#show', as: 'our_growers', defaults: { id: 4 }
  get 'plant-care', to: 'pages#show', as: 'plant_care', defaults: { id: 5 }
  get 'frequently-asked-questions', to: 'pages#show', as: 'faq', defaults: { id: 3 }
  get 'contact-us', to: 'contacts#new', as: 'contact_us'
  get 'terms-and-policies', to: 'pages#show', as: 'terms', defaults: { id: 7 }
  get 'sitemap', to: 'pages#show', as: 'sitemap', defaults: { id: 6 }
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
