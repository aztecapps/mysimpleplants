# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Mysimpleplants::Application.config.secret_key_base = 'bf0f14f79726be8cf949dc3952784bf382ac9ddfe15aef889b8099f601ec2f4ddfe922d8fc1882e94b4c30c187d5511114567090ae7976b968da4baa5536b5db'
