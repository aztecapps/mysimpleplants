require 'test_helper'

class GrowersControllerTest < ActionController::TestCase
  setup do
    @grower = growers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:growers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create grower" do
    assert_difference('Grower.count') do
      post :create, grower: { name: @grower.name, region_id: @grower.region_id }
    end

    assert_redirected_to grower_path(assigns(:grower))
  end

  test "should show grower" do
    get :show, id: @grower
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @grower
    assert_response :success
  end

  test "should update grower" do
    patch :update, id: @grower, grower: { name: @grower.name, region_id: @grower.region_id }
    assert_redirected_to grower_path(assigns(:grower))
  end

  test "should destroy grower" do
    assert_difference('Grower.count', -1) do
      delete :destroy, id: @grower
    end

    assert_redirected_to growers_path
  end
end
