require 'test_helper'

class GrowerZipCodesControllerTest < ActionController::TestCase
  setup do
    @grower_zip_code = grower_zip_codes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:grower_zip_codes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create grower_zip_code" do
    assert_difference('GrowerZipCode.count') do
      post :create, grower_zip_code: { grower_id: @grower_zip_code.grower_id, zip_code_id: @grower_zip_code.zip_code_id }
    end

    assert_redirected_to grower_zip_code_path(assigns(:grower_zip_code))
  end

  test "should show grower_zip_code" do
    get :show, id: @grower_zip_code
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @grower_zip_code
    assert_response :success
  end

  test "should update grower_zip_code" do
    patch :update, id: @grower_zip_code, grower_zip_code: { grower_id: @grower_zip_code.grower_id, zip_code_id: @grower_zip_code.zip_code_id }
    assert_redirected_to grower_zip_code_path(assigns(:grower_zip_code))
  end

  test "should destroy grower_zip_code" do
    assert_difference('GrowerZipCode.count', -1) do
      delete :destroy, id: @grower_zip_code
    end

    assert_redirected_to grower_zip_codes_path
  end
end
