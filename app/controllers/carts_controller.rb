class CartsController < InheritedResources::Base

  def show
    if !params[:promo_code].blank?
      self.applyPromo(params[:promo_code])
    elsif !session[:promo_code].blank?
      self.applyPromo(session[:promo_code])
    end
    @promo_code = session[:promo_code] ? session[:promo_code] : ''
    @cart = current_cart  
    @plants = Plant.all.collect{ |p| [p.name, p.id] }
    respond_to do |format|
      format.html
      format.json { render json: @cart }
    end
  end

  def update_quantity
    line_item = LineItem.find(params[:line_item_id])
    quantity = params[:quantity].to_i
    if quantity < 1
      quantity = 1
    end
    line_item.quantity = quantity
    line_item.product_subtotal = quantity * line_item.product_price
    line_item.save!
    cart = current_cart
    cart.set_subtotal
    cart_update = {subtotal: cart.subtotal.to_f, tax: cart.tax.to_f, discount: cart.discount.to_f, total: cart.total.to_f, quantity: line_item.quantity, item_total: line_item.product_subtotal.to_f, }
    render json: cart_update
  end

  def update_hated_plant
    cart = current_cart
    cart.update_attribute(:hated_plant_id, params[:hated_plant_id])
    render json: {success: 1}
  end

  def applyPromo(promo_code)
    cart = current_cart
    promo = Promotion.new
    this_promo = promo.getPromo(promo_code)
    if this_promo.blank?
      flash.now[:error] = "Invalid Promo"
    else
      session[:promo_code] = promo_code
      cart.update_attribute(:promotion_id, this_promo.id)
      current_cart.set_subtotal
      flash.now[:notice] = "Promo Accepted"
    end
  end

  def update_totals
    zip_code = ZipCode.where('zip_code = ?', params[:zip_code]).first
    @cart = current_cart
    @cart.update_attribute(:tax_rate, zip_code.tax)
    @cart.set_subtotal
    render layout: false
  end

end
