class OrdersController < InheritedResources::Base

  force_ssl if: :ssl_configured?

  def new
    @order = current_cart.build_order(session[:order])
  end

  def create
    @order = current_cart.build_order(order_params)
    @order.ip_address = request.remote_ip
    @order.grower_id = ZipCode.findGrower(@order.zip)
    session_params = order_params
    session_params[:card_number] = ''
    session_params[:card_verification] = ''
    session[:order] = session_params
    if @order.save
      if @order.purchase
        @order.schedule_pickup if @order.grower_id.present?
        @shipping_message = ZipCode.get_shipping_message(@order.zip)
        OrderMailer.receipt_email(@order).deliver
        OrderMailer.notification_email(@order).deliver
        session[:cart_id] = nil        
        render action: "confirm"
      else
        @ccerr = true
        render action: "new"
      end
    else
      render action: "new"
    end
  end

  def update
    @order = Order.find(params[:id])
    session_params = order_params
    session_params[:card_number] = ''
    session_params[:card_verification] = ''
    session[:order] = session_params
    if @order.update_attributes(order_params)
      if @order.purchase
        @order.schedule_pickup if @order.grower_id.present?
        @shipping_message = ZipCode.get_shipping_message(@order.zip)
        OrderMailer.receipt_email(@order).deliver
        OrderMailer.notification_email(@order).deliver
        session[:cart_id] = nil
        render action: "confirm"
      else
        @ccerr = true
        render action: "new"
      end
    else
      render action: "new"
    end
  end

  private

  def order_params
    params.require(:order).permit(:first_name, :last_name, :address, :address2, :city, :state, :zip, :phone, :email, :billing_first_name, :billing_last_name, :billing_address, :billing_address2, :billing_city, :billing_state, :billing_zip, :billing_phone, :card_type, :card_expires_on, :card_number, :card_verification, :pickup_date)
  end

end
