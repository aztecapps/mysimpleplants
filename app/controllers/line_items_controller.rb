class LineItemsController < InheritedResources::Base

  def create
    @cart = current_cart
    product = Product.find(params[:product_id])
    @line_item = @cart.add_to_cart(product, params[:options], params[:quantity])
    respond_to do |format|
      if @line_item.save
        @cart.set_subtotal
        format.html { redirect_to @line_item.cart }
        format.json { render json: @line_item, status: :created, location: @line_item }
      else
        format.html { render action: "new" }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @line_item = LineItem.find(params[:id])

    respond_to do |format|
      if @line_item.update_attributes(params[:line_item])
        @cart.set_subtotal
        format.html { redirect_to @line_item, notice: 'Line item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    cart = current_cart
    @line_item = LineItem.where(id: params[:id]).where(cart_id: cart.id).first
    @line_item.destroy
    cart.set_subtotal

    respond_to do |format|
      format.html { redirect_to '/mycart' }
      format.json { head :no_content }
    end
  end

end