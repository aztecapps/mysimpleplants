class ZipCodesController < InheritedResources::Base

  def check_availability
  	if params[:zip_code].blank?
  	  render json: { message: "Zip code is blank. Please enter a zip code.", status: 2 }
  	else
	    zip_code = ZipCode.where('zip_code = ?', params[:zip_code]).first
	    cart = current_cart
	    if zip_code.blank?
	      render json: { message: "We're sorry, your zip code could not be found. Please try again, or feel free to complete your order and we will contact you for further information.", status: 0 }
	    elsif zip_code.zone.status == 0
	      render json: { message: "We're sorry, we are currently unable to deliver to your area. Please feel free to complete your order and we will send it as soon as we're able.", status: 0 }
	      #session[:zip] = zip_code.zip_code
	      #session[:tax] = zip_code.tax
              #cart.update_attribute(:tax_rate, zip_code.tax)
	      #current_cart.set_subtotal
            elsif zip_code.zone.ship_date > DateTime.now
              s_date = zip_code.zone.ship_date + 7.days
              ship_date =Date.strptime(s_date.to_s, "%Y-%m-%d").strftime("%m-%d-%Y")
              render json: { message: "Based on seasonality, orders start shipping to your area approximately #{ship_date}. Place your order now to receive 50% off and we won't charge your credit card until your plants ship. We will send a separate shipping confirmation email that contains your tracking number when your plants ship.", status: 1 }
              #session[:zip] = zip_code.zip_code
              #cart.update_attribute(:tax_rate, zip_code.tax)
              #current_cart.set_subtotal
	    else
	      render json: { message: "Your order will arrive via FedEx in 5-7 days.", status: 1 }
	      #session[:zip] = zip_code.zip_code
              #cart.update_attribute(:tax_rate, zip_code.tax)
	      #current_cart.set_subtotal
	    end
	  end
  end

end
