class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_cart, :mobile_device?

  before_filter :apply_promo

  def after_sign_in_path_for(resource)
    growers_home_path
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end

  def apply_promo
    if params[:pc].present?
      session[:promo_code] = params[:pc]
    end
  end


  protected

  def check_for_mobile
    session[:mobile_override] = params[:mobile] if params[:mobile]
    prepare_for_mobile if mobile_device?
  end

  def prepare_for_mobile
    prepend_view_path Rails.root + 'app' + 'views_mobile'
  end

  def ssl_configured?
    !Rails.env.development?
  end

  def mobile_device?
    if session[:mobile_override]
      session[:mobile_override] == "1"
    else
      (request.user_agent =~ /Mobile|webOS/) && (request.user_agent !~ /iPad/)
    end
  end

  def current_cart
    cart = Cart.find_by_id(session[:cart_id])
    if cart.blank?
      cart = Cart.create
      #cart.account_id = current_account.id if current_account
      cart.set_subtotal
      session[:cart_id] = cart.id
    end
    cart
  end

  def current_order
    if session[:order].blank?
      session[:order] = {}
    end
    session[:order]
  end

end
