class ContactsController < InheritedResources::Base

  force_ssl if: :ssl_configured?

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
    	ContactMailer.notification_email(@contact).deliver
      render action: 'thanks'
    else
      render action: 'new'
    end
  end

  def thanks
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :subject, :message)
  end

end
