class RegistrationsController < InheritedResources::Base

  force_ssl if: :ssl_configured?

  def new
    @registration = Registration.new
  end

  def create
    @registration = Registration.new(registration_params)
    if @registration.save
      RegistrationMailer.notification_email(@registration).deliver
      redirect_to action: 'thanks'
    else
      render action: 'new'
    end
  end

  def thanks
    session[:promo_code] = 'msp201450'  
    current_cart.update_attribute(:promotion_id, 2)
    current_cart.set_subtotal
  end

  private

  def registration_params
    params.require(:registration).permit(:first_name, :last_name, :email, :zip_code)
  end

end
