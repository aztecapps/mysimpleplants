class GrowersController < InheritedResources::Base

  before_filter :authenticate_grower!

  def show
    vals = defaults
    @grower = Grower.find(current_grower.id)
    @totals = Order.totals(current_grower.id, vals[:from_date], vals[:to_date])
    @table = Order.filter_orders(current_grower.id, vals[:from_date], vals[:to_date], vals[:order_filter])
  end

  def edit_info
    @grower = Grower.find(current_grower.id)
  end

  def show_order
    @order = Order.find(params[:order_id])
    render 'growers/orders/show'
  end

  def totals
    vals = defaults
    @totals = Order.totals(current_grower.id, vals[:from_date], vals[:to_date])
    render partial: 'totals', object: @totals
  end

  def order_status_complete
  	if params[:orders].present?
      params[:orders].each do |o|
        @order = Order.find(o)
        @order.update_attribute(:status, "Complete")
      end
    end
    @grower = Grower.find(current_grower.id)
    vals = defaults
    @table = Order.filter_orders(current_grower.id, vals[:from_date], vals[:to_date], vals[:order_filter])
    render partial: 'table', object: @table
  end

  def order_status_picked
    if params[:orders].present?
      params[:orders].each do |o|
        @order = Order.find(o)
        @order.update_attribute(:status, "Picked")
      end
    end
    @grower = Grower.find(current_grower.id)
    vals = defaults
    @table = Order.filter_orders(current_grower.id, vals[:from_date], vals[:to_date], vals[:order_filter])
    render partial: 'table', object: @table
  end

  def table
    @grower = Grower.find(current_grower.id)
    vals = defaults
    @table = Order.filter_orders(current_grower.id, vals[:from_date], vals[:to_date], vals[:order_filter])
    render partial: 'table', object: @table
  end

  def shipping_label
    @order = Order.find(params[:order_id])
    send_data(Base64.decode64(@order.shipping_label), :type => 'application/pdf', :filename => "label.pdf", :disposition => "inline")
  end

  def master_picking_sheet
    if params[:orders].present?
      @grower = Grower.find(current_grower.id)
      @items = Order.get_master_picking_items(params[:orders])
    end
    render layout: false
  end

  def picking_sheets
    if params[:orders].present?
      @grower = Grower.find(current_grower.id)
      @items = Order.get_picking_items(params[:orders])
    end
    render layout: false
  end

  def shipping_labels
  	if params[:orders].present?
  		@orders = Order.find(params[:orders])
  	end
  	#if(@orders.present?)
  	#	pdfs = ''
  	#	@orders.each do |o|
  	#		pdfs = "#{pdfs}#{o.shipping_label}"
  	#	end
  	#end
  	#send_data(Base64.decode64(pdfs), :type => 'application/pdf', :filename => "label.pdf", :disposition => "inline")
  	render layout: false
  end

  def update
    @grower = Grower.find(current_grower.id)
    if @grower.update_attributes(grower_params)
      flash[:notice] = 'Your information was successfully updated.'
      redirect_to growers_home_path
    else
      render :action => 'edit_info'
    end
  end

  private

  def defaults
    ps = table_params
    if ps.blank?
    	ps = {}
    	ps[:order_filter] = ''
	    ps[:from_date] = '1980-01-01'
	    ps[:to_date] = '2999-01-01'
    else
	    if ps[:order_filter].blank?
	    	ps[:order_filter] = ''
	    end
	    if ps[:from_date].blank?
	    	ps[:from_date] = '1980-01-01'
	    end
	    if ps[:to_date].blank?
	    	ps[:to_date] = '2999-01-01'
	    end
	  end
    ps
  end

  def table_params
  	params.permit(:orders, :order_filter, :from_date, :to_date)
  end

  def grower_params
    params.require(:grower).permit(:name, :address, :address2, :city, :state, :zip, :phone, :email)
  end

end
