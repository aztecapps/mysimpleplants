class RegistrationMailer < ActionMailer::Base

  def notification_email(registration)
    @registration = registration
    mail(to: "admin@mysimpleplants.com", from: 'service@mysimpleplants.com', subject: "mysimpleplants.com registration")
  end

end
