class ContactMailer < ActionMailer::Base

  def notification_email(contact)
    @contact = contact
    mail(to: "admin@mysimpleplants.com", from: "service@mysimpleplants.com", subject: @contact.subject)
  end

end
