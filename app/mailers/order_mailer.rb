class OrderMailer < ActionMailer::Base

  def receipt_email(order)
    @order = order
    mail(to: @order.email, from: 'service@mysimpleplants.com', subject: "mysimpleplants.com Order # #{@order.id}")
  end

  def notification_email(order)
    @order = order
    mail(to: 'admin@mysimpleplants.com', from: 'service@mysimpleplants.com', subject: "mysimpleplants.com Order # #{@order.id}")
  end

end
