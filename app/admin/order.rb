ActiveAdmin.register Order do

  controller do
    def permitted_params
      params.permit!
    end
  end

 
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  #permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  #end

  index do
    selectable_column
    column :id
    column :ip_address
    column :first_name
    column :last_name
    column :email
    column :phone
    column :address
    column :address2
    column :city
    column :state
    column :zip
    column :billing_first_name
    column :billing_last_name
    column :billing_phone
    column :billing_address
    column :billing_address2
    column :billing_city
    column :billing_state
    column :billing_zip
    column :card_type
    column "Exp. Date" do |order|
      order.card_expires_on.strftime("%m/%Y")
    end
    column "Subtotal" do |order|
      number_to_currency order.cart.subtotal if order.cart.present?
    end
    column "Tax" do |order|
      number_to_currency order.cart.tax if order.cart.present?
    end
    column "Discount" do |order|
      number_to_currency order.cart.discount if order.cart.present?
    end
    column "Total" do |order|
      number_to_currency order.cart.total if order.cart.present?
    end
    column "Promo Code" do |order|
      order.cart.promotion.promo_code if order.cart.present? && order.cart.promotion.present?
    end
    column :status
    column "PayPal Auth" do |order|
      order.transactions[0].authorization if order.transactions.present?
    end
    column "Total Boxes" do |order|
      order.cart.line_items.sum(:quantity) if order.cart.present? && order.cart.line_items.present?
    end
    column "Grower" do |order|
      order.grower.name if order.grower.present?
    end
    column "FexEx Conf #", :confirmation_code
    column :tracking_number
    column "Pickup Date", sortable: :pickup_date do |order|
      order.pickup_date.strftime("%m/%d/%Y at %I:%M%p") if order.pickup_date.present?
    end
    column "Created On", sortable: :created_at do |order|
      order.created_at.strftime("%m/%d/%Y at %I:%M%p")
    end
    column "Updated On", sortable: :updated_at do |order|
      order.updated_at.strftime("%m/%d/%Y at %I:%M%p")
    end
    column "Do Not Want" do |order|
      if order.cart.plant.present?
        order.cart.plant.name
      end
    end
    actions
  end

  csv do
    column :id
    column :ip_address
    column :first_name
    column :last_name
    column :email
    column :phone
    column :address
    column :address2
    column :city
    column :state
    column :zip
    column :billing_first_name
    column :billing_last_name
    column :billing_phone
    column :billing_address
    column :billing_address2
    column :billing_city
    column :billing_state
    column :billing_zip
    column :card_type
    column "Exp. Date" do |order|
      order.card_expires_on.strftime("%m/%Y")
    end
    column "Subtotal" do |order|
      number_to_currency order.cart.subtotal if order.cart.present?
    end
    column "Tax" do |order|
      number_to_currency order.cart.tax if order.cart.present?
    end
    column "Discount" do |order|
      number_to_currency order.cart.discount if order.cart.present?
    end
    column "Total" do |order|
      number_to_currency order.cart.total if order.cart.present?
    end
    column "Promo Code" do |order|
      order.cart.promotion.promo_code if order.cart.present? && order.cart.promotion.present?
    end
    column :status
    column "Auth Code" do |order|
      order.transactions[0].authorization if order.transactions.present?
    end
    column "Total Boxes" do |order|
      order.cart.line_items.sum(:quantity) if order.cart.present? && order.cart.line_items.present?
    end
    column "Grower" do |order|
      order.grower.name if order.grower.present?
    end
    column :confirmation_code
    column :tracking_number
    column "Pickup Date" do |order|
      order.pickup_date.strftime("%m/%d/%Y at %I:%M%p") if order.pickup_date.present?
    end
    column "Created On" do |order|
      order.created_at.strftime("%m/%d/%Y at %I:%M%p")
    end
    column "Updated On" do |order|
      order.updated_at.strftime("%m/%d/%Y at %I:%M%p")
    end
    column "Do Not Want" do |order|
      if order.cart.plant.present?
        order.cart.plant.name
      end
    end
  end

  show do |ad|
    attributes_table do
      row :id
      row :ip_address
      row :first_name
      row :last_name
      row :email
      row :phone
      row :address
      row :address2
      row :city
      row :state
      row :zip
      row :billing_first_name
      row :billing_last_name
      row :billing_phone
      row :billing_address
      row :billing_address2
      row :billing_city
      row :billing_state
      row :billing_zip
      row :card_type
      row "Exp. Date" do |order|
        order.card_expires_on.strftime("%m/%Y")
      end
      row "Subtotal" do |order|
        number_to_currency order.cart.subtotal if order.cart.present?
      end
      row "Tax" do |order|
        number_to_currency order.cart.tax if order.cart.present?
      end
      row "Discount" do |order|
        number_to_currency order.cart.discount if order.cart.present?
      end
      row "Total" do |order|
        number_to_currency order.cart.total if order.cart.present?
      end
      row "Promo Code" do |order|
         order.cart.promotion.promo_code if order.cart.present? && order.cart.promotion.present?
      end
      row :status
      row "PayPal Auth" do |order|
        order.transactions[0].authorization if order.transactions.present?
      end
      row "Grower" do |order|
        order.grower.name if order.grower.present?
      end
      row "FexEx Conf #", :confirmation_code
      row :tracking_number
      row "Pickup Date" do |order|
        order.pickup_date.strftime("%m/%d/%Y at %I:%M%p") if order.pickup_date.present?
      end
      row "Created On" do |order|
        order.created_at.strftime("%m/%d/%Y at %I:%M%p")
      end
      row "Updated On" do |order|
        order.updated_at.strftime("%m/%d/%Y at %I:%M%p")
      end
      row "Do Not Want" do |order|
        if order.cart.plant.present?
          order.cart.plant.name
        end
      end
      if order.cart.line_items.present?
        table_for order.cart.line_items do
          column :product
          column "Options" do |line_item|
            raw line_item.line_item_options.map { |li| "#{li.option_set_name}: #{li.option_name}"  }.compact.join('<br>')
          end
          column "Exclude" do |line_item|
            if line_item.cart.plant.present?
              line_item.cart.plant.name
            end
          end
          column :quantity
          column :product_price
          column :product_subtotal
        end
      end
    end
  end
  
end
