json.array!(@options) do |option|
  json.extract! option, :id, :name, :price, :option_set_id, :order_num
  json.url option_url(option, format: :json)
end
