json.array!(@option_sets) do |option_set|
  json.extract! option_set, :id, :name, :product_id, :order_num
  json.url option_set_url(option_set, format: :json)
end
