json.array!(@line_item_options) do |line_item_option|
  json.extract! line_item_option, :id, :line_item_id, :option_id, :option_name, :option_price, :option_set_id, :option_set_name
  json.url line_item_option_url(line_item_option, format: :json)
end
