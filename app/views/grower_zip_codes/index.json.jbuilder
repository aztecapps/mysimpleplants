json.array!(@grower_zip_codes) do |grower_zip_code|
  json.extract! grower_zip_code, :id, :zip_code_id, :grower_id
  json.url grower_zip_code_url(grower_zip_code, format: :json)
end
