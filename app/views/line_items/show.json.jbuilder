json.extract! @line_item, :id, :cart_id, :product_id, :product_name, :product_price, :quantity, :product_subtotal, :created_at, :updated_at
