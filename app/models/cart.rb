# == Schema Information
#
# Table name: carts
#
#  id           :integer          not null, primary key
#  account_id   :integer
#  created_at   :datetime
#  updated_at   :datetime
#  subtotal     :decimal(, )
#  tax          :decimal(, )
#  discount     :decimal(, )
#  total        :decimal(, )
#  promotion_id :integer
#  tax_rate     :decimal(, )
#  plant_id     :integer
#

class Cart < ActiveRecord::Base

  has_one :order
  belongs_to :plant
  has_many :line_items
  belongs_to :promotion

  accepts_nested_attributes_for :line_items

  def add_to_cart(product, options, quantity)
    if quantity.blank? || quantity.to_i < 1
      quantity = 1
    end
    options.reject!(&:empty?)
    options.map!(&:to_s)
    cart_product = self.return_item(product, options)
    if cart_product.present?
      cart_product.quantity = cart_product.quantity + quantity.to_i
      cart_product.save
    else
      cart_product = line_items.build(product_id: product.id, product_name: product.name, product_price: product.price, quantity: quantity.to_i)
      if options.present?
        options.each do |o|
          o = Option.find(o)
          cart_product.line_item_options.build(option_id: o.id, option_name: o.name, option_price: o.price, option_set_id: o.option_set.id, option_set_name: o.option_set.name)
        end
      end
    end
    self.set_subtotal
    cart_product
  end

  def set_subtotal
    subtotal = 0
    self.line_items.each do |li|
      subtotal += li.product_subtotal.to_f
    end
    self.update_attribute(:subtotal, subtotal)
    if self.promotion.present?
      self.update_attribute(:discount, (self.promotion.amount * 0.01) * self.subtotal)
    else
      self.update_attribute(:discount, 0);
    end
    if(self.tax_rate.present?)
      self.update_attribute(:tax, (self.subtotal - self.discount) * self.tax_rate)
    else
      self.update_attribute(:tax, 0);  
    end
    self.update_attribute(:total, self.subtotal - self.discount + self.tax)
  end

  def return_item(product, options)
    current_item = self.line_items.select('line_items.*').joins(:line_item_options).where('line_items.product_id = ? and line_item_options.option_id IN (?)', product.id, options).group('line_items.id').having('COUNT(distinct line_item_options.option_id) = ?', options.length).first
    return current_item
  end

end
