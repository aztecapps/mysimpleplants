# == Schema Information
#
# Table name: zip_codes
#
#  id         :integer          not null, primary key
#  zip_code   :string(255)
#  zone_id    :integer
#  created_at :datetime
#  updated_at :datetime
#  tax        :decimal(, )
#  grower_id  :integer
#  state      :string(255)
#

class ZipCode < ActiveRecord::Base

  belongs_to :zone
  has_one :grower
  has_many :orders, primary_key: :zip_code, foreign_key: :zip

  def self.findGrower zip
    harts = [4,5]
    dj = [1,2,3]
    z = ZipCode.where(zip_code: zip.to_s).first
    if z.present?
      if dj.include? z.grower_id
        return 2
      else
        return 4
      end
    end
  end

  def self.get_shipping_message(zip)
    zip_code = ZipCode.where('zip_code = ?', zip.to_s).first
    return "We're sorry, your zip code could not be found. We will contact you for further information." if zip_code.blank?
    if zip_code.zone.status == 0
      return "We're sorry, we are currently unable to deliver to your area. We will send it as soon as we're able."
    elsif zip_code.zone.ship_date > DateTime.now
      s_date = zip_code.zone.ship_date + 7.days
      ship_date =Date.strptime(s_date.to_s, "%Y-%m-%d").strftime("%m-%d-%Y")
      return "Based on seasonality, orders start shipping to your area approximately #{ship_date}"
    else
      return "Your order will arrive via FedEx in 5-7 days."
    end
  end

end
