# == Schema Information
#
# Table name: growers
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  address                :string(255)
#  address2               :string(255)
#  city                   :string(255)
#  state                  :string(255)
#  zip                    :string(255)
#  phone                  :string(255)
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  logo                   :string(255)
#
# Indexes
#
#  index_growers_on_reset_password_token  (reset_password_token) UNIQUE
#

class Grower < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :zip_codes, through: :grower_zip_codes
  has_many :orders

  def shipping_address
    {
      name: name,
      company: name,
      address: "#{address} #{address2}",
      city: city,
      state: state,
      postal_code: zip,
      country_code: "US",
      phone_number: phone
    }
  end

end
