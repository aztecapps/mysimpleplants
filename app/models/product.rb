# == Schema Information
#
# Table name: products
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  price      :decimal(, )
#

class Product < ActiveRecord::Base

  has_many :option_sets, order: 'order_num asc'
  has_many :steps, order: 'order_num asc'
  has_many :line_items

end
