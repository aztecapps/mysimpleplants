# == Schema Information
#
# Table name: promotions
#
#  id         :integer          not null, primary key
#  promo_code :string(255)
#  start_date :date
#  end_date   :date
#  amount     :decimal(, )
#  promo_type :string(255)
#

class Promotion < ActiveRecord::Base

  has_many :carts

  def getPromo(promo)
  	this_promo = Promotion.where("upper(promo_code) = upper(?) and CURRENT_TIMESTAMP between start_date AND end_date", promo).first
  	return this_promo
  end
  
end
