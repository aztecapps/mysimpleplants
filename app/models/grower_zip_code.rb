# == Schema Information
#
# Table name: grower_zip_codes
#
#  id          :integer          not null, primary key
#  zip_code_id :integer
#  grower_id   :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class GrowerZipCode < ActiveRecord::Base

  has_many :growers
  has_many :zip_codes

end
