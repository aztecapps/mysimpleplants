# == Schema Information
#
# Table name: line_item_options
#
#  id              :integer          not null, primary key
#  line_item_id    :integer
#  option_id       :integer
#  option_name     :string(255)
#  option_price    :decimal(, )
#  option_set_id   :integer
#  option_set_name :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class LineItemOption < ActiveRecord::Base

  belongs_to :line_item

end
