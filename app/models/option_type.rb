# == Schema Information
#
# Table name: option_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  template   :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class OptionType < ActiveRecord::Base

  has_many :option_sets

end
