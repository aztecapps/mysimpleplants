# == Schema Information
#
# Table name: registrations
#
#  id         :integer          not null, primary key
#  first_name :string(255)
#  last_name  :string(255)
#  email      :string(255)
#  created_at :datetime
#  updated_at :datetime
#  zip_code   :string(255)
#

class Registration < ActiveRecord::Base

  validates :first_name, :last_name, :email, presence: true

end
