# == Schema Information
#
# Table name: zones
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  status     :integer
#  ship_date  :date
#

class Zone < ActiveRecord::Base

  has_many :zip_codes
  has_many :orders, through: :zip_codes

end
