# == Schema Information
#
# Table name: option_sets
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  product_id     :integer
#  order_num      :integer
#  created_at     :datetime
#  updated_at     :datetime
#  step_id        :integer
#  default_value  :string(255)
#  option_type_id :integer
#

class OptionSet < ActiveRecord::Base

  has_many :options, order: 'order_num asc'
  belongs_to :product
  belongs_to :step
  belongs_to :option_type

end
