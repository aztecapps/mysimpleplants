# == Schema Information
#
# Table name: orders
#
#  id                 :integer          not null, primary key
#  cart_id            :integer
#  ip_address         :string(255)
#  first_name         :string(255)
#  last_name          :string(255)
#  address            :string(255)
#  address2           :string(255)
#  city               :string(255)
#  state              :string(255)
#  zip                :string(255)
#  phone              :string(255)
#  email              :string(255)
#  billing_address    :string(255)
#  billing_address2   :string(255)
#  billing_city       :string(255)
#  billing_state      :string(255)
#  billing_zip        :string(255)
#  card_type          :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  card_expires_on    :date
#  billing_first_name :string(255)
#  billing_last_name  :string(255)
#  billing_phone      :string(255)
#  grower_id          :integer
#  confirmation_code  :string(255)
#  tracking_number    :string(255)
#  status             :string(255)
#  shipping_label     :text
#  pickup_date        :datetime
#

class Order < ActiveRecord::Base

  default_scope order('created_at DESC')

  belongs_to :cart
  has_many :transactions, class_name: "OrderTransaction"
  belongs_to :grower
  belongs_to :zip_code, foreign_key: :zip, primary_key: :zip_code 
  has_one :zone, through: :zip_code

  attr_accessor :card_number, :card_verification
  
  validates :first_name, :last_name, :address, :city, :state, :zip, :phone, :email, :billing_first_name, :billing_last_name, :billing_address, :billing_city, :billing_state, :billing_zip, :billing_phone, :card_type, presence: true

  validates :card_expires_on, presence: true, on: :create
  validates :card_verification, presence: true, numericality: true, length: {minimum: 3, maximum: 4}, on: :create
  validate :validate_card, on: :create

  before_save :default_values

  def default_values
    self.status ||= 'Open'
  end

  def self.filter_orders(grower, from, to, filter)
    orders = []
    if grower.present?
      if filter.present?
        orders = Order.where(grower_id: grower, created_at: from..to, status: filter)
      else
        orders = Order.where(grower_id: grower, created_at: from..to)
      end
    end
    orders
  end

  def purchase
    response = GATEWAY.authorize(price_in_cents, credit_card, purchase_options)
    transactions.create!(action: "authorize", amount: price_in_cents, response: response)
    response.success?
  end
  
  def price_in_cents
    (cart.total * 100).round
  end

  def schedule_pickup
    if grower_id.present?
      grower = Grower.find(grower_id)
      pickup_date = get_pickup_date
      #s = FEDEX.ship(shipper: grower.shipping_address, recipient: shipping_address, packages: packages, service_type: 'FEDEX_GROUND', ready_date_time: pickup_date)
      s = FEDEX.ship(shipper: grower.shipping_address, recipient: shipping_address, packages: packages, service_type: 'FEDEX_GROUND')
      self.update_attributes(tracking_number: s[:completed_shipment_detail][:completed_package_details][:tracking_ids][:tracking_number], shipping_label: s[:completed_shipment_detail][:completed_package_details][:label][:parts][:image], pickup_date: pickup_date)
    end
  end

  private

  def get_pickup_date
    today = Date.today
    weekday = today.wday
    next_day_shipping = [1,2]
    if weekday == 0
      ship_date = Date.today.next_day(2)
    elsif next_day_shipping.include? weekday
      ship_date = Date.today.next_day(1)
    else
      ship_date = Date.today.next_week
    end
    ship_date.strftime("%Y-%m-%dT11:00:00-07:00")
  end

  def shipping_address
    {
      name: "#{first_name} #{last_name}",
      company: "",
      address: "#{address} #{address2}",
      city: city,
      state: state,
      postal_code: zip,
      country_code: "US",
      phone_number: phone,
      residential: true
    }
  end

  def packages 
    mypackages = []
    self.cart.line_items.each do |i|
      x = 0
      while x < i.quantity do
        mypackages << {
          weight: {
            units: "LB",
            value: 16
          },
          dimensions: {
            units: "IN",
            length: 16,
            width: 15,
            height: 21
          }
        }
        x = x + 1
      end
    end
    mypackages
  end

  def purchase_options
    {
      ip: ip_address,
      billing_address: {
        name:  "#{billing_first_name} #{billing_last_name}",
        address1: "#{billing_address} #{billing_address2}",
        city: billing_city,
        state: billing_state,
        country: "US",
        zip: billing_zip
      }
    }
  end

  def validate_card
    unless credit_card.valid?
      credit_card.errors.full_messages.each do |message|
        errors.add :base, message
      end
    end
  end

  def credit_card
    @credit_card ||= ActiveMerchant::Billing::CreditCard.new(
      :type               => card_type,
      :number             => card_number,
      :verification_value => card_verification,
      :month              => card_expires_on.month,
      :year               => card_expires_on.year,
      :first_name         => first_name,
      :last_name          => last_name
    )
  end

  def self.totals(grower, from, to)
    totals = {}
    all = Order.select("count(*) as total").where(grower_id: grower, created_at: from..to).reorder('').first
    totals[:all] = all.total
    open = Order.select("count(*) as total").where(grower_id: grower, status: ["Open", "Picked"], created_at: from..to).reorder('').first
    totals[:open] = open.total
    closed = Order.select("count(*) as total").where(grower_id: grower, status: "Complete", created_at: from..to).reorder('').first
    totals[:closed] = closed.total
    totals
  end

  def self.get_picking_items(order_ids)
    @items = {}
    @color1 = ''
    @color2 = ''
    @color3 = ''
    @sun = ''
    orders = Order.where(id: order_ids)
    orders.each do |order|
      @id = order.id
      @items[@id] = {}
      @items[@id]['items'] = {}
      @items[@id]['donotwant'] = order.cart.plant.name unless order.cart.plant.blank?
      order.cart.line_items.each do |l|
        if l.line_item_options.any?
          if l.line_item_options.size <= 2
            q = 12 * l.quantity
          elsif l.line_item_options.size == 3
            q = 6 * l.quantity
          else
            q = 4 * l.quantity
          end
          @color1 = ''
          @color2 = ''
          @color3 = ''
          l.line_item_options.each do |o|
            if o.option_set_id == 1
              @color1 = o.option_name
            elsif o.option_set_id == 2
              @sun = o.option_name
            elsif o.option_set_id == 4
              @color2 = o.option_name
            elsif o.option_set_id == 5
              @color3 = o.option_name
            end
          end
          if @color1 != ''
            if @items[@id]['items'][@sun].blank?
              @items[@id]['items'][@sun] = {}
              @items[@id]['items'][@sun][@color1] = q
            else
              current_q = @items[@id]['items'][@sun][@color1]
              if(current_q.to_i > 0)
                @items[@id]['items'][@sun][@color1] = current_q + q
              else
                @items[@id]['items'][@sun][@color1] = q
              end
            end
          end
          if @color2 != ''
            if @items[@id]['items'][@sun].blank?
              @items[@id]['items'][@sun] = {}
              @items[@id]['items'][@sun][@color2] = q
            else
              current_q = @items[@id]['items'][@sun][@color2]
              if(current_q.to_i > 0)
                @items[@id]['items'][@sun][@color2] = current_q + q
              else
                @items[@id]['items'][@sun][@color2] = q
              end
            end
          end
          if @color3 != ''
            if @items[@id]['items'][@sun].blank?
              @items[@id]['items'][@sun] = {}
              @items[@id]['items'][@sun][@color3] = q
            else
              current_q = @items[@id]['items'][@sun][@color3]
              if(current_q.to_i > 0)
                @items[@id]['items'][@sun][@color3] = current_q + q
              else
                @items[@id]['items'][@sun][@color3] = q
              end
            end
          end
        end
      end
    end
    @items
  end

  def self.get_master_picking_items(order_ids)
    @items = {}
    @color1 = ''
    @color2 = ''
    @color3 = ''
    @sun = ''
    orders = Order.where(id: order_ids)
    orders.each do |order|
      order.cart.line_items.each do |l|
        if l.line_item_options.any?
          if l.line_item_options.size <= 2
            q = 12 * l.quantity
          elsif l.line_item_options.size == 3
            q = 6 * l.quantity
          else
            q = 4 * l.quantity
          end
          @color1 = ''
          @color2 = ''
          @color3 = ''
          l.line_item_options.each do |o|
            if o.option_set_id == 1
              @color1 = o.option_name
            elsif o.option_set_id == 2
              @sun = o.option_name
            elsif o.option_set_id == 4
              @color2 = o.option_name
            elsif o.option_set_id == 5
              @color3 = o.option_name
            end
          end
          if @color1 != ''
            if @items[@sun].blank?
              @items[@sun] = {}
              @items[@sun][@color1] = q
            else
              current_q = @items[@sun][@color1]
              if(current_q.to_i > 0)
                @items[@sun][@color1] = current_q + q
              else
                @items[@sun][@color1] = q
              end
            end
          end
          if @color2 != ''
            if @items[@sun].blank?
              @items[@sun] = {}
              @items[@sun][@color2] = q
            else
              current_q = @items[@sun][@color2]
              if(current_q.to_i > 0)
                @items[@sun][@color2] = current_q + q
              else
                @items[@sun][@color2] = q
              end
            end
          end
          if @color3 != ''
            if @items[@sun].blank?
              @items[@sun] = {}
              @items[@sun][@color3] = q
            else
              current_q = @items[@sun][@color3]
              if(current_q.to_i > 0)
                @items[@sun][@color3] = current_q + q
              else
                @items[@sun][@color3] = q
              end
            end
          end
        end
      end
    end
    @items
  end

end
