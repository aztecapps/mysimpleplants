# == Schema Information
#
# Table name: steps
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#  order_num   :integer
#  product_id  :integer
#  tooltip     :text
#

class Step < ActiveRecord::Base

  belongs_to :product
  has_many :option_sets, order: 'order_num asc'

end
