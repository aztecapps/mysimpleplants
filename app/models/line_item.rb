# == Schema Information
#
# Table name: line_items
#
#  id               :integer          not null, primary key
#  cart_id          :integer
#  product_id       :integer
#  product_name     :string(255)
#  product_price    :decimal(, )
#  quantity         :integer
#  product_subtotal :decimal(, )
#  created_at       :datetime
#  updated_at       :datetime
#

class LineItem < ActiveRecord::Base

  belongs_to :cart
  belongs_to :product
  has_many :line_item_options

  validates :cart_id, presence: true
  validates :product_id, presence: true
  validates :quantity, presence: true
  validates :product_price, presence: true
  validates :product_name, presence: true

  before_save do
    if self.changed.include? 'quantity' or self.changed.include? 'product_price'
      self.product_subtotal = (self.quantity * self.product_price).round(2)
    end
    return true
  end

  #after_save do
  #  self.cart.set_subtotal
  #  return true
  #end

end
