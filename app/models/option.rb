# == Schema Information
#
# Table name: options
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  price         :decimal(, )
#  option_set_id :integer
#  order_num     :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Option < ActiveRecord::Base

  belongs_to :option_set

end
