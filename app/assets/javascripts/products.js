$(document).ready(function() {

  $("form").bind("keydown, keyup, keypress", function(e) {
   if (e.keyCode === 13) return false;
  });

  var isVisible = false;

  var hideAllPopovers = function() {
     $('i').each(function() {
          $(this).popover('hide');
      });  
  };

	$("#zip_search").focus();

  $('i').popover({ html: true, trigger: 'manual' }).on("click", function(e) {
    if(isVisible) {
      hideAllPopovers();
    }
    $(this).popover('show');
    $('.popover').off('click').on('click', function(e) {
        e.stopPropagation();
    });
    isVisible = true;
    e.stopPropagation();
  });

  $(document).on('click', function(e) {
      hideAllPopovers();
      isVisible = false;
  });

  $("#zip_search").on("focusout", function() {
    if( $("#zip_search").val() != '' ) {
    	$(".alert").remove();
    	$.getJSON('/zip_codes/check_availability.json', 'zip_code=' + $(this).val(), function(data) {
    		if( data.status == '1' ) {
    			div_class = 'alert alert-zip pull-left';
    		} else if( data.status == '0' ) {
  				div_class = 'alert alert-warning pull-left';
  			} else {
  				div_class = 'alert alert-danger pull-left';
  				$("#zip_search").focus();
  				$(window).scrollTop(0);
  		  }
  			d = $('<div>').addClass(div_class).css('width', '100%').html(data.message);
  			$("#step_1").after(d);
  		});
    }
  });

  $("#product_form").on('submit', function(){
  	$(".alert").remove();
  	div_class = 'alert alert-danger';
  	var d = $('<div>').addClass(div_class);
  	if( $("#zip_search").val() == '') {
  		d.html('Zip code is required.');
  		$("#step_1").before(d);
  		$("#zip_search").focus();
  		$(window).scrollTop(0);
  		return false;
  	}
  	var required = true;
  	$.each($('select[name="options[]"]'), function(i, v){
              if($(this).attr('title') != 'Second Color Choice' && $(this).attr('title') != 'Third Color Choice') {
  		if($(this).val() === null || $(this).val() == '') {
  			d.html($(this).attr('title') + ' is required.');
  			$("#step_1").before(d);
  			required = false;
  			$(this).focus();
  			return required;
  		}
	      }
  	});
  	if(required === false)
  		$(window).scrollTop(0);
  	return required;
  });

});
