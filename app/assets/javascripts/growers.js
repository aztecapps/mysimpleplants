$(document).ready( function() {
  $(".tablesorter").tablesorter();
  $(".datepicker").datepicker();
  $(".datepicker").on('blur', function() {
    $('#grower_table_wrapper').load('/growers/table', $("#grower_form").serializeArray());
    $('#grower_totals_wrapper').load('/growers/totals', $("#grower_form").serializeArray());
  });
  $("#order_filter").on('change', function() {
    $('#grower_table_wrapper').load('/growers/table', $("#grower_form").serializeArray());
  });
  $("#filter_orders").on('click', function(){
  	action = $("#order_action").val();
    if(action == '1') {
      $('#grower_table_wrapper').load('/growers/order_status_picked', $("#grower_form").serializeArray());
    } else if(action == '2') {
			$('#grower_table_wrapper').load('/growers/order_status_complete', $("#grower_form").serializeArray());
  	} else if(action == '3') {
  		$.post('/growers/master_picking_sheet',$("#grower_form").serializeArray(), function(data) {
  			var w = window.open();
  			$(w.document.body).html(data);
  			w.print();
  		})
  	} else if(action == '4') {
  		$.post('/growers/shipping_labels',$("#grower_form").serializeArray(), function(data) {
  			var w = window.open();
  			$(w.document.body).html(data);
  			w.print();
  		})
  	} else if(action == '5') {
                $.post('/growers/picking_sheets',$("#grower_form").serializeArray(), function(data) {
                        var w = window.open();
                        $(w.document.body).html(data);
                        w.print();
                })
        }
  	return false;
  });
  
  $("#print_order").on('click', function(){
    $.post('/growers/picking_sheets',$("#grower_order_form").serializeArray(), function(data) {
      var w = window.open();
      $(w.document.body).html(data);
      w.print();
    });
    return false;
  });

});
