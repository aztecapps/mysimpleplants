$(document).ready(function() {

	var updateFields = function() {
		$("#order_first_name").val($("#order_billing_first_name").val());
		$("#order_last_name").val($("#order_billing_last_name").val());
		$("#order_address").val($("#order_billing_address").val());
		$("#order_address2").val($("#order_address_address2").val());
		$("#order_city").val($("#order_billing_city").val());
		$("#order_state").val($("#order_billing_state").val());
		$("#order_zip").val($("#order_billing_zip").val());
		$("#order_phone").val($("#order_billing_phone").val());
	}

        var update_totals = function(zip) {
          $.get('/carts/update_totals','zip_code=' + zip, function(data){
	    $("#order_totals").html(data);
          });
        }

        $('#order_zip').on('focusout', function() {
          update_totals($(this).val());
	});

	$('#order_billing_zip').on('focusout', function() {
	  if($('#shipping_same_as_billing').is(':checked')) {
	    update_totals($(this).val());
	  }
        });

	$("form#new_order").on('submit', function() {
	  //e.preventDefault();
  	  $('.alert').remove();
          $("div").removeClass('has-error');
	  //$("#shipping_address_fields").slideDown();
	  if($('#shipping_same_as_billing').is(':checked')) {
	    updateFields();
	  }
	  update_totals($('#order_zip').val());
	  var required = true;
  	  $.each($('.required'), function(i, v){
   	    if($(this).val() == '') {
  	      $(this).closest('.form-group').addClass('has-error');
  	      required = false;
  	    }
  	  });
  	  if(required === false) {
   	    div_class = 'alert alert-danger';
  	    var d = $('<div>').addClass(div_class).html('The fields in red are required.');
  	    $('h1').after(d);
  	    $(window).scrollTop(0);
  	  }
  	  return required;
	});

	$('#shipping_same_as_billing').on('click', function() {
		if($(this).is(':checked')) {
			updateFields();
			$("#shipping_address_fields").slideUp();
		} else {
			$("#shipping_address_fields").slideDown();
		}
	});

});
