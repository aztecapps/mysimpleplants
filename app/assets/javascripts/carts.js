$(document).ready(function() {

	$(".quantity").on('input', function() {
	  $(".saved").remove();
	  $(this).siblings('div.update').html('<a href="#" class="update_link" >Update</a>');
	});
  
  $("#cart-page").on('click', 'a.update_link', function() {
	  var update = $(this);
	  var qty = $(this).parent('div.update').siblings('.quantity');
	  var price = $(this).closest('div.col-md-3').siblings('div.col-md-2');//.children('td.item_subtotal');
	  var quantity = $(qty).val();
	  var item = $(qty).attr('item');
	  $.getJSON('/carts/update_quantity.json', 'quantity=' + quantity + '&line_item_id=' + item, function(data) {
		  $(qty).val(data.quantity);
		  $(price).html('$' + (data.item_total * 1).toFixed(2));
		  $("#cart_subtotal").html('$' + (data.subtotal * 1).toFixed(2));
		  $("#cart_discount").html('$' + (data.discount * 1).toFixed(2));
		  $("#cart_tax").html('$' + (data.tax * 1).toFixed(2));
		  $("#cart_total").html('$' + (data.total * 1).toFixed(2));
		  $(update).parent('div.update').html('<span class="saved" style="color:#59AE4A;">Saved</span>');
		});
	  return false;
	});

	$("#cart-page #apply_promo").on('click', function() {
		if($('#promo_code').val() == '') {
			alert("Promo code field is blank.");
			return false;
		}
	});

  $("#plant_id").on('change', function() {
  	$.getJSON('/carts/update_hated_plant.json', 'hated_plant_id=' + $(this).val(), function(data) {});
  });

});
