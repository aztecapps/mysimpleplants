class AddAddressToGrowers < ActiveRecord::Migration
  def change
    add_column :growers, :address, :string
    add_column :growers, :address2, :string
    add_column :growers, :city, :string
    add_column :growers, :state, :string
    add_column :growers, :zip, :string
    add_column :growers, :phone, :string
    add_column :growers, :email, :string
  end
end
