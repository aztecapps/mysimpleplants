class RemoveRegionIdFromGrowers < ActiveRecord::Migration
  def change
    remove_column :growers, :region_id, :integer
  end
end
