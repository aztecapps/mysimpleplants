class AddHatedPlantIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :hated_plant_id, :integer
  end
end
