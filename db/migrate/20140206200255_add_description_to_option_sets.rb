class AddDescriptionToOptionSets < ActiveRecord::Migration
  def change
    add_column :option_sets, :description, :text
  end
end
