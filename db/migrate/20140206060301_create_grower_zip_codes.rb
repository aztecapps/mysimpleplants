class CreateGrowerZipCodes < ActiveRecord::Migration
  def change
    create_table :grower_zip_codes do |t|
      t.integer :zip_code_id
      t.integer :grower_id

      t.timestamps
    end
  end
end
