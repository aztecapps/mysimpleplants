class AddBillingLastNameToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :billing_last_name, :string
  end
end
