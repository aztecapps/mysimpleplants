class AddGrowerIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :grower_id, :integer
  end
end
