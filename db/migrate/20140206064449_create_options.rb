class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.string :name
      t.decimal :price
      t.integer :option_set_id
      t.integer :order_num

      t.timestamps
    end
  end
end
