class ChangeHatedPlantId < ActiveRecord::Migration
  def change
    rename_column :carts, :hated_plant_id, :plant_id
  end
end
