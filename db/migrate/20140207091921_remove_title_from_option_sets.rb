class RemoveTitleFromOptionSets < ActiveRecord::Migration
  def change
    remove_column :option_sets, :title, :string
  end
end
