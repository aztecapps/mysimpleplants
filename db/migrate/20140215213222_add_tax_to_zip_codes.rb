class AddTaxToZipCodes < ActiveRecord::Migration
  def change
    add_column :zip_codes, :tax, :decimal
  end
end
