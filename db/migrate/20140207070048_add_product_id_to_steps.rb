class AddProductIdToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :product_id, :integer
  end
end
