class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.integer :cart_id
      t.integer :product_id
      t.string :product_name
      t.decimal :product_price
      t.integer :quantity
      t.decimal :product_subtotal

      t.timestamps
    end
  end
end
