class AddDefaultValueToOptionSets < ActiveRecord::Migration
  def change
    add_column :option_sets, :default_value, :string
  end
end
