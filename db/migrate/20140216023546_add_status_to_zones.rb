class AddStatusToZones < ActiveRecord::Migration
  def change
    add_column :zones, :status, :integer
  end
end
