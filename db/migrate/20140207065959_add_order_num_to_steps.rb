class AddOrderNumToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :order_num, :integer
  end
end
