class AddShipDateToZones < ActiveRecord::Migration
  def change
    add_column :zones, :ship_date, :date
  end
end
