class AddTaxRateToCarts < ActiveRecord::Migration
  def change
    add_column :carts, :tax_rate, :decimal
  end
end
