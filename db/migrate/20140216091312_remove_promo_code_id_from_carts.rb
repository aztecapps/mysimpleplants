class RemovePromoCodeIdFromCarts < ActiveRecord::Migration
  def change
    remove_column :carts, :promo_code_id, :integer
  end
end
