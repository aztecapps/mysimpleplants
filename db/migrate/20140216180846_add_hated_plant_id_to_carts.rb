class AddHatedPlantIdToCarts < ActiveRecord::Migration
  def change
    add_column :carts, :hated_plant_id, :integer
  end
end
