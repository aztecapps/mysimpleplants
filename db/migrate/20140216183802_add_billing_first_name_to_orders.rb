class AddBillingFirstNameToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :billing_first_name, :string
  end
end
