class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :cart_id
      t.string :ip_address
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :phone
      t.string :email
      t.string :billing_address
      t.string :billing_address2
      t.string :billing_city
      t.string :billing_state
      t.string :billing_zip
      t.string :card_type
      t.date :card_expires_on

      t.timestamps
    end
  end
end
