class AddBillingPhoneToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :billing_phone, :string
  end
end
