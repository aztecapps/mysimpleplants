class CreateGrowers < ActiveRecord::Migration
  def change
    create_table :growers do |t|
      t.string :name
      t.integer :region_id

      t.timestamps
    end
  end
end
