class RemoveZoneFromZipCodes < ActiveRecord::Migration
  def change
    remove_column :zip_codes, :zone, :string
  end
end
