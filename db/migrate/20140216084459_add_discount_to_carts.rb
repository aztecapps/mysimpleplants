class AddDiscountToCarts < ActiveRecord::Migration
  def change
    add_column :carts, :discount, :decimal
  end
end
