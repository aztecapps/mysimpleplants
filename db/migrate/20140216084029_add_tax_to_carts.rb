class AddTaxToCarts < ActiveRecord::Migration
  def change
    add_column :carts, :tax, :decimal
  end
end
