class RemoveDescriptionFromOptionSets < ActiveRecord::Migration
  def change
    remove_column :option_sets, :description, :text
  end
end
