class AddStepIdToOptionSets < ActiveRecord::Migration
  def change
    add_column :option_sets, :step_id, :integer
  end
end
