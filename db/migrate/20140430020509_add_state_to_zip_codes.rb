class AddStateToZipCodes < ActiveRecord::Migration
  def change
    add_column :zip_codes, :state, :string
  end
end
