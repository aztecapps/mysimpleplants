class CreateOptionSets < ActiveRecord::Migration
  def change
    create_table :option_sets do |t|
      t.string :name
      t.integer :product_id
      t.integer :order_num

      t.timestamps
    end
  end
end
