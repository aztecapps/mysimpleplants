class AddTooltipToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :tooltip, :text
  end
end
