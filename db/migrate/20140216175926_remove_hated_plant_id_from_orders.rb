class RemoveHatedPlantIdFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :hated_plant_id, :integer
  end
end
