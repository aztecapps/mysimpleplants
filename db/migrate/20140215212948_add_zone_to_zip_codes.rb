class AddZoneToZipCodes < ActiveRecord::Migration
  def change
    add_column :zip_codes, :zone, :string
  end
end
