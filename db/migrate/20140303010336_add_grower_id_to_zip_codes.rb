class AddGrowerIdToZipCodes < ActiveRecord::Migration
  def change
    add_column :zip_codes, :grower_id, :integer
  end
end
