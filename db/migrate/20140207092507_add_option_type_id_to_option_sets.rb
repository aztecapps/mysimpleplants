class AddOptionTypeIdToOptionSets < ActiveRecord::Migration
  def change
    add_column :option_sets, :option_type_id, :integer
  end
end
