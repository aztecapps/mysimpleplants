class AddPromotionIdToCarts < ActiveRecord::Migration
  def change
    add_column :carts, :promotion_id, :integer
  end
end
