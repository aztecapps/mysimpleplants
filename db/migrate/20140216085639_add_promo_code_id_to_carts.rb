class AddPromoCodeIdToCarts < ActiveRecord::Migration
  def change
    add_column :carts, :promo_code_id, :integer
  end
end
