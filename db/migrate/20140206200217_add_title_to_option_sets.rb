class AddTitleToOptionSets < ActiveRecord::Migration
  def change
    add_column :option_sets, :title, :string
  end
end
