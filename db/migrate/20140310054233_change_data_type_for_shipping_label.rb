class ChangeDataTypeForShippingLabel < ActiveRecord::Migration
  def self.up
    change_table :orders do |t|
      t.change :shipping_label, :text
    end
  end
end
